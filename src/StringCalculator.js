class StringCalculator {

  separator = ',';

  add(stringInput) {
    let truncatedString = this.setCustomSeparator(stringInput).trim();

    // let truncatedString = stringInput.trim();
    if (truncatedString.length === 0) return 0;
    else if (truncatedString.substr(-1) === this.separator) throw new Error('String cannot end with empty entry');
    else if (truncatedString.indexOf(this.separator) > -1) return this.calculateSum(truncatedString);
    else return parseInt(truncatedString, 10);
  }

  setCustomSeparator(stringInput) {
    if (stringInput.substr(0, 2) === '//') {
      const sepMatch = stringInput.match(/\/\/([^\n]+)\n/);
      this.separator = (sepMatch && sepMatch[1]) || ',';
      return stringInput.substr(this.separator.length + 3);
    } else return stringInput;
  }

  calculateSum(valueString) {
    const operands = valueString.split(this.separator);
    return operands.reduce(this.addToTotal(operands), 0);
  }

  addToTotal(operands) {
    return (acc, val) => {
      if (val < 0) {
        const error = this.composeNegativeNumberErrorMsg(operands);
        throw new Error(error);
      }
      return acc + parseFloat(val);
    }
  }

  composeNegativeNumberErrorMsg(operands) {
    const errorNumbers = operands.filter(x => x < 0)
      .join(', ');

    return `Negative numbers ${errorNumbers} not permitted`;
  }
}

module.exports = StringCalculator;
