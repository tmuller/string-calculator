const StringCalculator = require('../src/StringCalculator');

describe('StringCalculator', () => {
  describe('add()', () => {

    let classUnderTest;

    beforeEach(() => {
      classUnderTest = new StringCalculator();
    })

    it('should return 0 for an empty string', () => {
      const result = classUnderTest.add('');
      expect(result).toEqual(0);
    });

    it('should return the passed number if there is only one number', () => {
      const result = classUnderTest.add('4');
      expect(result).toEqual(4);
    });

    it('should add two numbers separated by commas', () => {
      const result = classUnderTest.add('4,6');
      expect(result).toEqual(10);
    });

    it('should accept a random number of arguments to add', () => {
      const result = classUnderTest.add('4,6,10,21,34,45,1');
      expect(result).toEqual(121);
    });

    it('should only accept positive numbers', () => {
      const testFct = () => {
        const result = classUnderTest.add('4,6,10,21,-34,45,1');
      }
      expect(testFct).toThrow();
    });

    it('should list negative numbers in the error message', () => {
      const testFct = () => {
        const result = classUnderTest.add('4,-6,10,21,-34,45,1');
      }
      expect(testFct).toThrowError('Negative numbers -6, -34 not permitted');
    });

    it('should throw an error if last argument is empty', () => {
      const testFct = () => {
        const result = classUnderTest.add('4,6,10,21,34,45,1,');
      }
      expect(testFct).toThrowError('String cannot end with empty entry');
    });

    it('should not be affected by leading whitespace', () => {
      const result = classUnderTest.add('  4,6,10,21,34,45,1');
      expect(result).toEqual(121);
    });

    it('should not be affected by trailing whitespace', () => {
      const result = classUnderTest.add('  4,6,10,21,34,45,1  ');
      expect(result).toEqual(121);
    });

    it('should not be affected by trailing whitespace when trialing comma', () => {
      const testFct = () => {
        const result = classUnderTest.add('4,6,10,21,34,45,1,   ');
      }
      expect(testFct).toThrowError('String cannot end with empty entry');
    });

    it('should add floating point numbers', () => {
      const result = classUnderTest.add('4.5,6.2');
      expect(result).toEqual(10.7);
    });

    it('should support custom, one-letter separators for numbers', () => {
      const result = classUnderTest.add('//;\n4.5;6.2');
      expect(result).toEqual(10.7);
    });

    it('should support multi-character separators', () => {
      const result = classUnderTest.add('//|#|\n4.5|#|6.2');
      expect(result).toEqual(10.7);
    });
  });
});
